from flask import Flask, render_template

import os.path as path
app = Flask(__name__)

def forbidden_url(url):
	if url[0]=="/" or "//" in url or ".." in url or "~" in url:
		return True
	return False

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.route("/<path:filepath>")
def file_server(filepath=None):
	if forbidden_url(filepath):
		return render_template("403.html"), 403
		
	internal_path = path.join("templates/", filepath)
	file_exists = path.exists(internal_path)
	if file_exists:
		return render_template(filepath), 200
	else:
		return render_template("404.html"), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
